
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>welcome</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link rel="stylesheet" href="assets/vendor/icofont/font-awesome.min.css">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="assets/vendor/aos/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="form.css" >


    <!-- Template Main CSS File -->

    <link href="assets/css/style.css" rel="stylesheet">

    <!-- =======================================================
    * Template Name: Gp - v2.0.0
    * Template URL: https://bootstrapmade.com/gp-free-multipurpose-html-bootstrap-template/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
</head>

<body>

<!-- ======= Header ======= -->
<header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center justify-content-between">

        <h1 class="logo"><a href="index.php">LifeRecruitment<span>.</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html" class="logo"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li class="active"><a href="home.php">Home</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#services">Services</a></li>
                <li><a href="#portfolio">Portfolio</a></li>
                <li><a href="#team">Team</a></li>
                <li class="drop-down"><a href="">Drop Down</a>
                    <ul>
                        <li><a href="#">Drop Down 1</a></li>
                        <li class="drop-down"><a href="#">Deep Drop Down</a>
                            <ul>
                                <li><a href="#">Deep Drop Down 1</a></li>
                                <li><a href="#">Deep Drop Down 2</a></li>
                                <li><a href="#">Deep Drop Down 3</a></li>
                                <li><a href="#">Deep Drop Down 4</a></li>
                                <li><a href="#">Deep Drop Down 5</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Drop Down 2</a></li>
                        <li><a href="#">Drop Down 3</a></li>
                        <li><a href="#">Drop Down 4</a></li>
                    </ul>
                </li>
                <li><a href="#contact">Contact</a></li>

            </ul>
        </nav><!-- .nav-menu -->

        <a href="registration.php" class="get-started-btn scrollto">Get Started</a>

    </div>
</header><!-- End Header -->






<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center justify-content-center">
    <div class="container" data-aos="fade-up">

        <div class="row justify-content-center" data-aos="fade-up" data-aos-delay="150">
            <div class="col-xl-6 col-lg-8">
                <h1>Powerful Nursing Agency Solutions With LRA<span>.</span></h1>
                <h2>We are team of talented nursing agency recruiters</h2>
            </div>
        </div>

        <div class="row mt-5 justify-content-center" data-aos="zoom-in" data-aos-delay="250">
            <div class="col-xl-2 col-md-4 col-6">
                <div class="icon-box">
                    <i class="ri-store-line"></i>
                    <h3><a href="">Accountability</a></h3>
                </div>
            </div>
            <div class="col-xl-2 col-md-4 col-6 ">
                <div class="icon-box">
                    <i class="ri-bar-chart-box-line"></i>
                    <h3><a href="">Reliability</a></h3>
                </div>
            </div>
            <div class="col-xl-2 col-md-4 col-6 mt-4 mt-md-0">
                <div class="icon-box">
                    <i class="ri-calendar-todo-line"></i>
                    <h3><a href="">Integrity</a></h3>
                </div>
            </div>
            <div class="col-xl-2 col-md-4 col-6 mt-4 mt-xl-0">
                <div class="icon-box">
                    <i class="ri-paint-brush-line"></i>
                    <h3><a href="">Confidentiality</a></h3>
                </div>
            </div>
            <div class="col-xl-2 col-md-4 col-6 mt-4 mt-xl-0">
                <div class="icon-box">
                    <i class="ri-database-2-line"></i>
                    <h3><a href="">Creditable</a></h3>
                </div>
            </div>
        </div>

    </div>
</section><!-- End Hero -->

<section class="intro py-5 bg-light" data-aos="fade-up">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="intro-box w-100 d-flex">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <i class="icofont-phone"></i>
                    </div>
                    <div class="text pl-3">
                        <a href="tel:+234-814-401-6484"> <h4 class="mb-0">Call us:+234-814-401-6484</h4></a>
                        <span class="alltext"> 198 West 21th Street, Suite 721 Abuja 10016</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="intro-box w-100 d-flex">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <i class="icofont-clock-time"></i>
                    </div>
                    <div class="text pl-3">
                        <h4 class="mb-0">Opening Hours</h4>
                        <span class="alltext">Mon - Sat 7:00 AM - 8:00 PM / Sundays closed</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="intro-box w-100">
                    <!-- Calendly link widget begin -->
                    <link href="https://assets.calendly.com/assets/external/widget.css" rel="stylesheet">
                    <script src="https://assets.calendly.com/assets/external/widget.js" type="text/javascript"></script>
                    <a href="" onclick="Calendly.initPopupWidget({url: 'https://calendly.com/fadiggah/30min'});return false;"> <p class="mb-0"><b class="btn btn-primary">Make an Appointment</b></p></a>
                    <!-- Calendly link widget end -->

                </div>
            </div>
        </div>
    </div>
</section>


<!--Start of welcome section-->
<section id="welcome" data-aos="fade-up" class="bg-img bg-fixed bg-overlay"style="background-image: url('')";>
    <div class="container"data-aos="zoom-in" data-aos-delay="250">
        <div class="row">
            <div class="col-md-12">
                <div class="wel_header">
                    <h2>welcome to LRA</h2>
                    <p>Life Recruitment agency. Offer shifts and opportunities that tailor to your schedule and personal needs
                        Provide the right support you need to develop and succeed in your career</p>
                </div>
            </div>
        </div>
        <!--End of row-->
        <div class="row">
            <div class="col-md-3"data-aos="zoom-in" data-aos-delay="200">
                <div class="item">
                    <div class="single_item">
                        <div class="item_list">
                            <div class="welcome_icon">
                                <i class="bx bx-world"></i>
                            </div>
                            <h4>Personal Care service/companionship</h4>
                            <p>We are determined to provide world class care. this includes our personal home sevice companionship for our patients.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--End of col-md-3-->
            <div class="col-md-3"data-aos="zoom-in" data-aos-delay="300">
                <div class="item">
                    <div class="single_item">
                        <div class="item_list">
                            <div class="welcome_icon">
                                <i class="bx bx-file"></i>
                            </div>
                            <h4>Skiled Nursing Service</h4>
                            <p>We make sure each client is is assesed in depth, conducted by our skilled nurses care services.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--End of col-md-3-->
            <div class="col-md-3"data-aos="zoom-in" data-aos-delay="400">
                <div class="item">
                    <div class="single_item">
                        <div class="item_list">
                            <div class="welcome_icon">
                                <i class="bx bx-arch"></i>
                            </div>
                            <h4>Rehabilitative Services</h4>
                            <p>our highly skilled rehabilitation specialist collaborate to develop individualized and evidence based rehab plan .</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--End of col-md-3-->
            <div class="col-md-3"data-aos="zoom-in" data-aos-delay="500">
                <div class="item">
                    <div class="single_item">
                        <div class="item_list">
                            <div class="welcome_icon">
                                <i class="bx bx-shield"></i>
                            </div>
                            <h4>Medical Care Services</h4>
                            <p>Our medical services are based on continuum of holistic health care that promotes maintains and restore health.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--End of col-md-3-->
        </div>
        <!--End of row-->
    </div>
    <!--End of container-->
</section>
<!--end of welcome section-->


<!--Start of volunteer-->
<section id="volunteer">
    <div class="container">
        <div class="row vol_area">
            <div class="col-md-8">
                <div class="volunteer_content">
                    <h3>Become a <span>Volunteer</span></h3>
                    <p>Join LRA And Help the world. </p>
                </div>
            </div>
            <!--End of col-md-8-->
            <div class="col-md-3 col-md-offset-1">
                <div class="join_us">
                    <a href="registration.php" class="vol_cust_btn">join us</a>
                </div>
            </div>
            <!--End of col-md-3-->
        </div>
        <!--End of row and vol_area-->
    </div>
    <!--End of container-->
</section>
<!--end of volunteer-->


<main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
        <div class="container" data-aos="fade-up">

            <div class="row">
                <!-- Card -->
                <div class="card  col-md-6" data-aos="fade-left" data-aos-delay="100" style="background-image: url(https://mdbootstrap.com/img/Photos/Horizontal/Work/4-col/img%20%2814%29.jpg);">

                    <!-- Content -->
                    <div class="rgba-black-strong py-5 px-4">
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-10 col-xl-8">

                                <!--Accordion wrapper-->
                                <div class="accordion md-accordion accordion-5" id="accordionEx5" role="tablist"
                                     aria-multiselectable="true">

                                    <!-- Accordion card -->
                                    <div class="card mb-4">

                                        <!-- Card header -->
                                        <div class="card-header p-0 z-depth-1" role="tab" id="heading30">
                                            <a data-toggle="collapse" data-parent="#accordionEx5" href="#collapse30" aria-expanded="true"
                                               aria-controls="collapse30">
                                                <i class="fas fa-cloud fa-2x p-3 mr-4 float-left black-text" aria-hidden="true"></i>
                                                <h4 class="text-uppercase white-text mb-0 py-3 mt-1">
                                                    Nursing Service
                                                </h4>
                                            </a>
                                        </div>

                                        <!-- Card body -->
                                        <div id="collapse30" class="collapse show" role="tabpanel" aria-labelledby="heading30"
                                             data-parent="#accordionEx5">
                                            <div class="card-body rgba-black-light white-text z-depth-1">
                                                <p class="p-md-4 mb-0">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus
                                                    terry richardson ad squid. 3 wolf moon officia aute,
                                                    non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch
                                                    3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                                                    shoreditch et.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Accordion card -->

                                    <!-- Accordion card -->
                                    <div class="card mb-4">

                                        <!-- Card header -->
                                        <div class="card-header p-0 z-depth-1" role="tab" id="heading31">
                                            <a data-toggle="collapse" data-parent="#accordionEx5" href="#collapse31" aria-expanded="true"
                                               aria-controls="collapse31">
                                                <i class="fas fa-comment-alt fa-2x p-3 mr-4 float-left black-text" aria-hidden="true"></i>
                                                <h4 class="text-uppercase white-text mb-0 py-3 mt-1">
                                                    Care giver service
                                                </h4>
                                            </a>
                                        </div>

                                        <!-- Card body -->
                                        <div id="collapse31" class="collapse" role="tabpanel" aria-labelledby="heading31"
                                             data-parent="#accordionEx5">
                                            <div class="card-body rgba-black-light white-text z-depth-1">
                                                <p class="p-md-4 mb-0">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus
                                                    terry richardson ad squid. 3 wolf moon officia aute,
                                                    non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch
                                                    3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                                                    shoreditch et.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Accordion card -->

                                    <!-- Accordion card -->
                                    <div class="card mb-4">

                                        <!-- Card header -->
                                        <div class="card-header p-0 z-depth-1" role="tab" id="heading32">
                                            <a data-toggle="collapse" data-parent="#accordionEx5" href="#collapse32" aria-expanded="true"
                                               aria-controls="collapse32">
                                                <i class="fas fa-cogs fa-2x p-3 mr-4 float-left black-text" aria-hidden="true"></i>
                                                <h4 class="text-uppercase white-text mb-0 py-3 mt-1">
                                                    Cleaning
                                                </h4>
                                            </a>
                                        </div>

                                        <!-- Card body -->
                                        <div id="collapse32" class="collapse" role="tabpanel" aria-labelledby="heading32"
                                             data-parent="#accordionEx5">
                                            <div class="card-body rgba-black-light white-text z-depth-1">
                                                <p class="p-md-4 mb-0">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus
                                                    terry richardson ad squid. 3 wolf moon officia aute,
                                                    non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch
                                                    3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                                                    shoreditch et.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Accordion card -->
                                </div>
                                <!--/.Accordion wrapper-->

                            </div>
                        </div>
                    </div>
                    <!-- Content -->
                </div>
                <!-- Card -->

                <div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content" data-aos="fade-right" data-aos-delay="100">
                    <h3>At Life Recruitment Agency, We are all about delivering a high quality and world-class standard of nursing and homecare</h3>
                    <p class="font-italic">
                        "The life insurance recruitment Agency is a full-service international placement Agency that works in conjunction with training institution for health care providers in the Philippines. We work together as a team to provide standard manpower from the Philippines for various job positions for health care providers overseas.Adequate resources are channeled towards providing optimum satisfaction to both our recruitment candidates and our sponsoring organization as well"
                    </p>
                    <ul>
                        <li><i class="ri-check-double-line"></i>Registered migration agent with the Migration Agents Registration Authority (MARA).</li>
                        <li><i class="ri-check-double-line"></i> A member of the Migration Institute of Nigeria Limited (MIA).</li>
                        <li><i class="ri-check-double-line"></i>    LIA is the peak professional body of migration agents in Nigeria appointed by the federal government</li>
                        <li><i class="ri-check-double-line"></i>   LIA was appointed to help consumers maintain high standards of: knowledge, ethics professionalism</li>

                    </ul>
                    <p>
                        We have years experience in securing visas for our clients, and a new life in a new country. Our staff have specialised legal and professional qualifications and expertise in immigration law. We have developed excellent working relationships with government immigration departments and embassies Our knowledge and track record will help you to present your application to successfully secure your visa.
                    </p>
                </div>
            </div>

        </div>
    </section><!-- End About Section -->

    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients">
        <div class="container" data-aos="zoom-in">

            <div class="owl-carousel clients-carousel">
                <img src="assets/img/clients/client-1.png" alt="">
                <img src="assets/img/clients/client-2.png" alt="">
                <img src="assets/img/clients/client-3.png" alt="">
                <img src="assets/img/clients/client-4.png" alt="">
                <img src="assets/img/clients/client-5.png" alt="">
                <img src="assets/img/clients/client-6.png" alt="">
                <img src="assets/img/clients/client-7.png" alt="">
                <img src="assets/img/clients/client-8.png" alt="">
            </div>

        </div>
    </section><!-- End Clients Section -->

    <!-- ======= Features Section ======= -->
    <section id="features" class="features">
        <div class="container" data-aos="fade-up">

            <div class="row">
                <div class="image col-lg-6" style='background-image: url("assets/img/features.jpg");' data-aos="fade-right"></div>
                <div class="col-lg-6" data-aos="fade-left" data-aos-delay="100">
                    <div class="icon-box mt-5 mt-lg-0" data-aos="zoom-in" data-aos-delay="150">
                        <i class="bx bx-receipt"></i>
                        <h4>Why Choose Us</h4>
                        <p>At LRA we are all about delivering high quality and world class standard of Talented Nurses from across the globe giving them a opportunity the want to be able to practise their profession </p>
                    </div>
                    <div class="icon-box mt-5" data-aos="zoom-in" data-aos-delay="150">
                        <i class="bx bx-cube-alt"></i>
                        <h4>Visa Procedure</h4>
                        <p>At LRA we are all about delivering high quality and world class standard of Talented Nurses from across the globe giving them a opportunity the want to be able to practise their profession</p>
                    </div>
                    <div class="icon-box mt-5" data-aos="zoom-in" data-aos-delay="150">
                        <i class="bx bx-images"></i>
                        <h4>Application Fee</h4>
                        <p>At LRA we are all about delivering high quality and world class standard of Talented Nurses from across the globe giving them a opportunity the want to be able to practise their profession</p>
                    </div>
                    <div class="icon-box mt-5" data-aos="zoom-in" data-aos-delay="150">
                        <i class="bx bx-shield"></i>
                        <h4>Service Procedure</h4>
                        <p>At LRA we are all about delivering high quality and world class standard of Talented Nurses from across the globe giving them a opportunity the want to be able to practise their profession</p>
                    </div>
                </div>
            </div>

        </div>
    </section><!-- End Features Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Services</h2>
                <p>Check our Services</p>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bxl-dribbble"></i></div>
                        <h4><a href="">Employer sponsored migration</a></h4>
                        <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in" data-aos-delay="200">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bx-file"></i></div>
                        <h4><a href="">Permanent Residence-Skilled Migration</a></h4>
                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0" data-aos="zoom-in" data-aos-delay="300">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bx-tachometer"></i></div>
                        <h4><a href="">Visitor and Tourist Visa</a></h4>
                        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="100">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bx-world"></i></div>
                        <h4><a href="">Family migration</a></h4>
                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="200">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bx-slideshow"></i></div>
                        <h4><a href="">Career Opportunities</a></h4>
                        <p>Quis consequatur saepe eligendi voluptatem consequatur dolor consequuntur</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="300">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bx-arch"></i></div>
                        <h4><a href="">Graduate Trannie Program</a></h4>
                        <p>Modi nostrum vel laborum. Porro fugit error sit minus sapiente sit aspernatur</p>
                    </div>
                </div>

            </div>

        </div>
    </section><!-- End Services Section -->




    <!-- ======= Cta Section ======= -->
    <section id="cta" class="cta">
        <div class="container" data-aos="zoom-in">

            <div class="text-center">
                <h3>Call To Action</h3>
                <p> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <a class="cta-btn" href="#">Call To Action</a>
            </div>

        </div>
    </section><!-- End Cta Section -->

    <!--start of event-->
    <section id="event">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="event_header text-center">
                        <h2>Trending articles</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                </div>
            </div>
            <!--End of row-->
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6 zero_mp">
                            <div class="event_item">
                                <div class="event_img">
                                    <img src="img/tree_cut_1.jpg" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 zero_mp">
                            <div class="event_item">
                                <div class="event_text text-center">
                                    <a href=""><h4>What career opportunities are available in Nigeria</h4></a>
                                    <h6>15-16 May in Philippines</h6>
                                    <p>Lorem ipsum dolor sit amet, consectetur adip scing elit. Lorem ipsum dolor sit amet,consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    <a href="" class="event_btn">read more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End of row-->
                    <div class="row">
                        <div class="col-md-6 zero_mp">
                            <div class="event_item">
                                <div class="event_text text-center">
                                    <a href=""><h4>How you can become a Registered Nurse in Nigeria</h4></a>
                                    <h6>15-16 May in Nigeria</h6>
                                    <p>Lorem ipsum dolor sit amet, consectetur adip scing elit. Lorem ipsum dolor sit amet,consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    <a href="" class="event_btn">read more</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 zero_mp">
                            <div class="event_item">
                                <div class="event_img">
                                    <img src="img/tree_cut_2.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End of row-->
                </div>
                <!--End of col-md-8-->
                <div class="col-md-4">
                    <div class="event_news">
                        <div class="event_single_item fix">
                            <div class="event_news_img floatleft">
                                <img src="img/tree_cut_3.jpg" alt="">
                            </div>
                            <div class="event_news_text">
                                <a href="#"><h4>I want to migrate to Nigeria...</h4></a>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, veniam.</p>
                            </div>
                        </div>
                    </div>
                    <div class="event_news">
                        <div class="event_single_item fix">
                            <div class="event_news_img floatleft">
                                <img src="img/tree_cut_4.jpg" alt="">
                            </div>
                            <div class="event_news_text">
                                <a href="#"><h4>Keep your house envirome..</h4></a>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, veniam.</p>
                            </div>
                        </div>
                    </div>
                    <div class="event_news">
                        <div class="event_single_item fix">
                            <div class="event_news_img floatleft">
                                <img src="img/tree_cut_3.jpg" alt="">
                            </div>
                            <div class="event_news_text">
                                <a href="#"><h4>Urgent Nurses Needed</h4></a>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, veniam.</p>
                            </div>
                        </div>
                    </div>
                    <div class="event_news">
                        <div class="event_single_item fix">
                            <div class="event_news_img floatleft">
                                <img src="img/tree_cut_4.jpg" alt="">
                            </div>
                            <div class="event_news_text">
                                <a href="#"><h4>One Tree Thousand Hope</h4></a>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, veniam.</p>
                            </div>
                        </div>
                    </div>
                    <div class="event_news">
                        <div class="event_single_item fix">
                            <div class="event_news_img floatleft">
                                <img src="img/tree_cut_3.jpg" alt="">
                            </div>
                            <div class="event_news_text">
                                <a href="#"><h4>How much will i earn</h4></a>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, veniam.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End of col-md-4-->
            </div>
            <!--End of row-->
        </div>
        <!--End of container-->
    </section>
    <!--end of event-->
    <div class="portfolio">
        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
                <img src="assets/img/portfolio/equipmet1.jpg" class="img-fluid" alt="">
                <div class="portfolio-info">
                    <h4>App 1</h4>
                    <p>App</p>
                    <div class="portfolio-links">
                        <a href="assets/img/portfolio/equipmet2.jpg" data-gall="portfolioGallery" class="venobox" title="App 1"><i class="bx bx-plus"></i></a>
                        <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Portfolio</h2>
                <p>Check our Portfolio</p>
            </div>

            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class="col-lg-12 d-flex justify-content-center">
                    <ul id="portfolio-flters">
                        <li data-filter="*" class="filter-active">All</li>
                        <li data-filter=".filter-app">App</li>
                        <li data-filter=".filter-card">Card</li>
                        <li data-filter=".filter-web">Web</li>
                    </ul>
                </div>
            </div>

            <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <div class="portfolio-wrap">
                        <img src="assets/img/portfolio/equipmet1.jpg" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>App 1</h4>
                            <p>App</p>
                            <div class="portfolio-links">
                                <a href="assets/img/portfolio/equipmet2.jpg" data-gall="portfolioGallery" class="venobox" title="App 1"><i class="bx bx-plus"></i></a>
                                <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                    <div class="portfolio-wrap">
                        <img src="assets/img/portfolio/equipmet3.jpg" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Web 3</h4>
                            <p>Web</p>
                            <div class="portfolio-links">
                                <a href="assets/img/portfolio/portfolio-2.jpg" data-gall="portfolioGallery" class="venobox" title="Web 3"><i class="bx bx-plus"></i></a>
                                <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <div class="portfolio-wrap">
                        <img src="assets/img/portfolio/hospital1.jpg" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>App 2</h4>
                            <p>App</p>
                            <div class="portfolio-links">
                                <a href="assets/img/portfolio/equipmet4.jpg" data-gall="portfolioGallery" class="venobox" title="App 2"><i class="bx bx-plus"></i></a>
                                <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                    <div class="portfolio-wrap">
                        <img src="assets/img/portfolio/hospital3.jpg" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Card 2</h4>
                            <p>Card</p>
                            <div class="portfolio-links">
                                <a href="assets/img/portfolio/portfolio-4.jpg" data-gall="portfolioGallery" class="venobox" title="Card 2"><i class="bx bx-plus"></i></a>
                                <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                    <div class="portfolio-wrap">
                        <img src="assets/img/portfolio/hospital2.jpg" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Web 2</h4>
                            <p>Web</p>
                            <div class="portfolio-links">
                                <a href="assets/img/portfolio/equipmet1.jpg" data-gall="portfolioGallery" class="venobox" title="Web 2"><i class="bx bx-plus"></i></a>
                                <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <div class="portfolio-wrap">
                        <img src="assets/img/portfolio/hospital3.jpg" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>App 3</h4>
                            <p>App</p>
                            <div class="portfolio-links">
                                <a href="assets/img/portfolio/equipmet2.jpg" data-gall="portfolioGallery" class="venobox" title="App 3"><i class="bx bx-plus"></i></a>
                                <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                    <div class="portfolio-wrap">
                        <img src="assets/img/portfolio/hospital1.jpg" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Card 1</h4>
                            <p>Card</p>
                            <div class="portfolio-links">
                                <a href="assets/img/portfolio/equipmet2.jpg" data-gall="portfolioGallery" class="venobox" title="Card 1"><i class="bx bx-plus"></i></a>
                                <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                    <div class="portfolio-wrap">
                        <img src="assets/img/portfolio/equipmet4.jpg" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Card 3</h4>
                            <p>Card</p>
                            <div class="portfolio-links">
                                <a href="assets/img/portfolio/hospital1.jpg" data-gall="portfolioGallery" class="venobox" title="Card 3"><i class="bx bx-plus"></i></a>
                                <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                    <div class="portfolio-wrap">
                        <img src="assets/img/portfolio/equipmet1.jpg" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Web 3</h4>
                            <p>Web</p>
                            <div class="portfolio-links">
                                <a href="assets/img/portfolio/equipmet1.jpg" data-gall="portfolioGallery" class="venobox" title="Web 3"><i class="bx bx-plus"></i></a>
                                <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section><!-- End Portfolio Section -->


    <!-- ======= Counts Section ======= -->
    <section id="counts" class="counts">
        <div class="container" data-aos="fade-up">

            <div class="row no-gutters">
                <div class="image col-xl-5 d-flex align-items-stretch justify-content-center justify-content-lg-start" data-aos="fade-right" data-aos-delay="100"></div>
                <div class="col-xl-7 pl-0 pl-lg-5 pr-lg-1 d-flex align-items-stretch" data-aos="fade-left" data-aos-delay="100">
                    <div class="content d-flex flex-column justify-content-center">
                        <h3>What our customers are saying</h3>
                        <p>
                            THIS WEBSITE WILL TELL YOU EVERYTHING YOU NEED TO KNOW, INCLUDING:
                        </p>
                        <div class="row">
                            <div class="col-md-6 d-md-flex align-items-md-stretch">
                                <div class="count-box">
                                    <i class="icofont-simple-smile"></i>
                                    <span data-toggle="counter-up">65</span>
                                    <p><strong>Happy Clients</strong> consequuntur voluptas nostrum aliquid ipsam architecto ut.</p>
                                </div>
                            </div>

                            <div class="col-md-6 d-md-flex align-items-md-stretch">
                                <div class="count-box">
                                    <i class="icofont-document-folder"></i>
                                    <span data-toggle="counter-up">85</span>
                                    <p><strong>Visa application</strong> adipisci atque cum quia aspernatur totam laudantium et quia dere tan</p>
                                </div>
                            </div>

                            <div class="col-md-6 d-md-flex align-items-md-stretch">
                                <div class="count-box">
                                    <i class="icofont-clock-time"></i>
                                    <span data-toggle="counter-up">4</span>
                                    <p><strong>Years of experience</strong> aut commodi quaerat modi aliquam nam ducimus aut voluptate non vel</p>
                                </div>
                            </div>

                            <div class="col-md-6 d-md-flex align-items-md-stretch">
                                <div class="count-box">
                                    <i class="icofont-award"></i>
                                    <span data-toggle="counter-up">1</span>
                                    <p><strong>Awards</strong> rerum asperiores dolor alias quo reprehenderit eum et nemo pad der</p>
                                </div>
                            </div>
                        </div>
                    </div><!-- End .content-->
                </div>
            </div>

        </div>
    </section><!-- End Counts Section -->

    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials">
        <div class="container" data-aos="zoom-in">

            <div class="owl-carousel testimonials-carousel">

                <div class="testimonial-item">
                    <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
                    <h3>Saul Goodman</h3>
                    <h4>Nurse</h4>
                    <p>
                        <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                        Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                        <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                </div>

                <div class="testimonial-item">
                    <img src="assets/img/testimonials/testimonials-2.jpg" class="testimonial-img" alt="">
                    <h3>Sara Wilsson</h3>
                    <h4>Care giver</h4>
                    <p>
                        <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                        Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
                        <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                </div>

                <div class="testimonial-item">
                    <img src="assets/img/testimonials/testimonials-3.jpg" class="testimonial-img" alt="">
                    <h3>Jena Karlis</h3>
                    <h4>Phamarcist</h4>
                    <p>
                        <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                        Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.
                        <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                </div>

                <div class="testimonial-item">
                    <img src="assets/img/testimonials/testimonials-4.jpg" class="testimonial-img" alt="">
                    <h3>Matt Brandon</h3>
                    <h4>Freelancer</h4>
                    <p>
                        <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                        Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.
                        <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                </div>

                <div class="testimonial-item">
                    <img src="assets/img/testimonials/testimonials-5.jpg" class="testimonial-img" alt="">
                    <h3>John Larson</h3>
                    <h4>Nurse</h4>
                    <p>
                        <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                        Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum quid.
                        <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                </div>

            </div>

        </div>
    </section><!-- End Testimonials Section -->

    <!-- ======= Team Section ======= -->
    <section id="team" class="team">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Team</h2>
                <p>Check our Team</p>
            </div>

            <div class="row">

                <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                    <div class="member" data-aos="fade-up" data-aos-delay="100">
                        <div class="member-img">
                            <img src="assets/img/team/avatar-png-2.png" class="img-fluid" alt="">
                            <div class="social">
                                <a href=""><i class="icofont-twitter"></i></a>
                                <a href=""><i class="icofont-facebook"></i></a>
                                <a href=""><i class="icofont-instagram"></i></a>
                                <a href=""><i class="icofont-linkedin"></i></a>
                            </div>
                        </div>
                        <div class="member-info">
                            <h4>John Ezenagu</h4>
                            <span>Chief Executive Officer</span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                    <div class="member" data-aos="fade-up" data-aos-delay="200">
                        <div class="member-img">
                            <img src="assets/img/team/avatar-png-2.png" class="img-fluid" alt="">
                            <div class="social">
                                <a href=""><i class="icofont-twitter"></i></a>
                                <a href=""><i class="icofont-facebook"></i></a>
                                <a href=""><i class="icofont-instagram"></i></a>
                                <a href=""><i class="icofont-linkedin"></i></a>
                            </div>
                        </div>
                        <div class="member-info">
                            <h4>Mary Jane</h4>
                            <span>Agent Advisor</span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                    <div class="member" data-aos="fade-up" data-aos-delay="300">
                        <div class="member-img">
                            <img src="assets/img/team/download.png" class="img-fluid" alt="">
                            <div class="social">
                                <a href=""><i class="icofont-twitter"></i></a>
                                <a href=""><i class="icofont-facebook"></i></a>
                                <a href=""><i class="icofont-instagram"></i></a>
                                <a href=""><i class="icofont-linkedin"></i></a>
                            </div>
                        </div>
                        <div class="member-info">
                            <h4>William Fadz</h4>
                            <span>CTO</span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                    <div class="member" data-aos="fade-up" data-aos-delay="400">
                        <div class="member-img">
                            <img src="assets/img/team/avatar-png-2.png" class="img-fluid" alt="">
                            <div class="social">
                                <a href=""><i class="icofont-twitter"></i></a>
                                <a href=""><i class="icofont-facebook"></i></a>
                                <a href=""><i class="icofont-instagram"></i></a>
                                <a href=""><i class="icofont-linkedin"></i></a>
                            </div>
                        </div>
                        <div class="member-info">
                            <h4>Amanda Jepson</h4>
                            <span>Accountant</span>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section><!-- End Team Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Contact</h2>
                <p>Contact Us</p>
            </div>

            <!--        <div>-->
            <!--          <iframe style="border:0; width: 100%; height: 270px;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" allowfullscreen></iframe>-->
            <!--        </div>-->

            <div class="row mt-5">

                <div class="col-lg-4">
                    <div class="info">
                        <div class="address">
                            <i class="icofont-google-map"></i>
                            <h4>Location:</h4>
                            <p>A108 Adam Street,Abuja, 535022</p>
                        </div>

                        <div class="email">
                            <i class="icofont-envelope"></i>
                            <h4>Email:</h4>
                            <p>ezenagu@gmail.com</p>
                        </div>

                        <div class="phone">
                            <i class="icofont-phone"></i>
                            <h4>Call:</h4>
                            <p>+234 81 444 016 48</p>
                        </div>

                    </div>

                </div>

                <div class="col-lg-8 mt-5 mt-lg-0">

                    <!--                    <form action="#" method="post" role="form" class="php-email-form">-->
                    <!--                        <div class="form-row">-->
                    <!--                            <div class="col-md-6 form-group">-->
                    <!--                                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />-->
                    <!--                                <div class="validate"></div>-->
                    <!--                            </div>-->
                    <!--                            <div class="col-md-6 form-group">-->
                    <!--                                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />-->
                    <!--                                <div class="validate"></div>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                        <div class="form-group">-->
                    <!--                            <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />-->
                    <!--                            <div class="validate"></div>-->
                    <!--                        </div>-->
                    <!--                        <div class="form-group">-->
                    <!--                            <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>-->
                    <!--                            <div class="validate"></div>-->
                    <!--                        </div>-->
                    <!--                        <div class="g-recaptcha" data-sitekey="6Lc-1AAVAAAAAHMUwW5ZjLjRjkSJ7Q1uLu4Op0OL"></div>-->
                    <!--                        <div class="mb-3">-->
                    <!--                            <div class="loading">Loading</div>-->
                    <!--                            <div class="error-message"></div>-->
                    <!--                            <div class="sent-message">--><!--</div>-->
                    <!--                        </div>-->
                    <!--                        <div class="text-center"><button type="submit" name="submit">Send Message</button></div>-->
                    <!--                    </form>-->







                    <form role="form" method="post" id="reused_form"class="php-email-form">

                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label for="name"> Your Name:</label>
                                <input type="text" class="form-control" placeholder="&#xf007;" id="name" name="name" required>
                            </div>
                            <div class="col-sm-6 form-group">
                                <label for="email"> Email:</label>
                                <input type="email" class="form-control" placeholder="&#xf003;" id="email" name="email" required>
                            </div>
                        </div>
                        <div class="form-row">

                            <div class="col-sm-12 form-group">
                                <label for="comments"> Comments:</label>
                                <textarea class="form-control" type="textarea" id="comments" placeholder="&#xf040;" name="comments" maxlength="6000" rows="7"></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <button type="submit" class="btn btn-lg btn-warning pull-right" > <i class="fa fa-send-o " aria-hidden="true"></i> Send &rarr;</button>
                            </div>
                        </div>
                    </form>



                    <div id="success_message"class="" style="width:100%; height:100%; display:none; ">succesfully sent we'll be in touch!</div>
                    <div id="error_message"class="error-message" style="width:100%; height:100%; display:none; "> <h3>Error</h3> Sorry there was an error sending your form. </div>



                </div>

            </div>

        </div>
    </section><!-- End Contact Section -->

</main><!-- End #main -->

<!-- ======= Footer ======= -->
<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6">
                    <div class="footer-info">
                        <h3>Life Recruitment<span>.</span></h3>
                        <p>
                            A108 Adam Street <br>
                            535022, Nigeria<br><br>
                            <strong>Phone:</strong> +234 814 401 6484<br>
                            <strong>Email:</strong> johnezenagu@gmail.com<br>
                        </p>
                        <div class="social-links mt-3">
                            <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                            <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                            <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                            <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                            <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-2 col-md-6 footer-links">
                    <h4>Useful Links</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Our Services</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Nursing Opportunies</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Agency Fee</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Visa Application</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Visa Process</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Client Service</a></li>
                    </ul>
                </div>

                <div class="col-lg-4 col-md-6 footer-newsletter">
                    <h4>Our Newsletter</h4>
                    <p>Subscribe to our news letter</p>
                    <form action="" method="post">
                        <input type="email" name="email"><input type="submit" value="Subscribe">
                    </form>

                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="copyright">
            &copy; Copyright <strong><span>LIfeRecruitmentAgency</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/gp-free-multipurpose-html-bootstrap-template/ -->
            Designed by <a href="#">FD</a>
        </div>
    </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
<div id="preloader"></div>

<!-- Vendor JS Files -->
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="assets/vendor/php-email-form/validate.js"></script>
<script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="assets/vendor/venobox/venobox.min.js"></script>
<script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
<script src="assets/vendor/counterup/counterup.min.js"></script>
<script src="assets/vendor/aos/aos.js"></script>



<!-- Template Main JS File -->

<script src="form.js"></script>
<script src="assets/js/main.js"></script>

</body>

</html>